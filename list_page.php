<?php 
include('header.php'); 
if(isset($_GET['cat']))
	{
	unset($_SESSION['category']);
	$cat = $_GET['cat'];
	if(isset($_SESSION['category']))
		{
		if (($key = array_search($cat, $_SESSION['category'])) !== false) 
			{
    		unset($_SESSION['category'][$key]);
			}
  		else
  			{
  			array_push($_SESSION['category'], $cat);
  			}
		}
	else
		{
		$array[] = $cat;
		$_SESSION['category'] = $array;
		}
	}
?>
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>collection</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">collection</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<section class="section-b-space ratio_asos" style="padding-top: 0px;">
	<div class="collection-wrapper">
	    <div class="container" id="refresh_div">
	        <div class="row">
	            <div class="col-sm-3 collection-filter">
	                <!-- side-bar colleps block stat -->
	                <div class="collection-filter-block">
	                    <!-- brand filter start -->
	                    <div class="collection-mobile-back">
	                        <span class="filter-back">
	                        	<i class="fa fa-angle-left" aria-hidden="true"></i> back
	                    	</span>
	                    </div>
	                    <div class="collection-collapse-block open">
	                        <h3 class="collapse-block-title">category</h3>
	                        <div class="collection-collapse-block-content">
	                            <div class="collection-brand-filter" id="categories">
	                                
	                        	</div>
	                    	</div>
	                	</div>
	            	</div>
	            </div>
	            <div class="collection-content col">
	                <div class="page-main-content">
	                   	<div class="row">
	                       	<div class="col-sm-12">
	                            <div class="collection-product-wrapper">
	                                <div class="product-top-filter">
	                                    <div class="row">
	                                        <div class="col-xl-12">
	                                           	<div class="filter-main-btn">
	                                            	<span class="filter-btn btn btn-theme"><i class="fa fa-filter" aria-hidden="true"></i> Filter</span>
	                                        	</div>
	                                    	</div>
	                                    </div>
	                                    <div class="product-wrapper-grid">
	                                        <div class="row margin-res" id="products">
	                                            
	                                    	</div>
	                                	</div>
	                            	</div>
	                        	</div>
	                    	</div>
	                	</div>
	            	</div>
	        	</div>
	    	</div>
		</div>
	</div>
</section>
<script type="text/javascript">
var category_session = [];
$(window).on('load', function () 
    {
    /*loadCategory();
	});
function loadCategory()
	{*/
	<?php 
	if(isset($_SESSION['category']))
		{
		foreach($_SESSION['category'] as $key => $val)
			{
			?>
			var val = '<?=$val;?>';
	    	category_session.push(val);
			<?php 
			}
		}
	?>
    // load categories
	$.ajax({
            url:'<?=$url;?>loadCategory',
            data:{merchant_keys:'<?=$merchant_keys;?>', device_id:'<?=$device_id;?>', device_platform:'<?=$device_platform;?>', device_uiid:'<?=$device_uiid;?>', code_version:'<?=$code_version;?>', lang:'<?=$lang;?>', search_mode:'<?=$search_mode;?>', location_mode:'<?=$location_mode;?>',},
			dataType:'json',
			async: false,
            success:function(result)
            	{
               	if(result.code == 1)
                	{
                    var cat = result.details.data;
                    var html_category = ""; 
                    var product_html = '';
					for (var i = 0; i < cat.length; i++) 
                    	{
                    	var checked = '';
                    	if (category_session.length > 0)
                    		{
                    		if(category_session.indexOf(cat[i].cat_id) != -1)
                				{
                				checked = 'checked';
                				$.ajax({
										url:'<?=$url;?>loadItemByCategory',
										async: false,
										data:{cat_id:cat[i].cat_id, merchant_keys:'<?=$merchant_keys;?>', device_id:'<?=$device_id;?>', device_platform:'<?=$device_platform;?>', device_uiid:'<?=$device_uiid;?>', code_version:'<?=$code_version;?>', lang:'<?=$lang;?>', search_mode:'<?=$search_mode;?>', location_mode:'<?=$location_mode;?>',},
										dataType:'json',
										success:function(result1)
											{
											if(result1.code == 1)
												{
												var product = result1.details.data;
												for(var j = 0; j < product.length; j++)
													{
													if(cart.indexOf(product[j].item_id) == -1)
														{
														var button = `<span id="cart_button_`+product[j].item_id+`"><button title="Add to cart" onclick="add_to_cart(`+product[j].item_id+`,`+product[j].prices[0].discount_price+`)"> <i class="ti-shopping-cart"></i></button></span>`;
														}
													else
														{
														var button = `<a href="cart.php" title="Go To Cart"><i class="ti-shopping-cart" style="color:red;"></i></a>`;
														}
													if(favorite.indexOf(product[j].item_id) == -1)
														{
			            								button = button+`<span id="favorite_button_`+product[j].item_id+`"><button onclick="add_to_fav(`+cat[i].cat_id+`,`+product[j].item_id+`)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button></span>`;
			            								}
			            							else
			            								{
			            								button = button+`<span id="favorite_button_`+product[j].item_id+`"><button onclick="remove_fav(`+product[j].item_id+`,`+cat[i].cat_id+`)" title="Remove from Wishlist"><i class="ti-heart" aria-hidden="true"  style="color:red;" ></i></button></span>`;
			            								}
			            							button = button+`<a href="product_view.php?product=`+product[j].item_id+`&cat=`+cat[i].cat_id+`" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>`;
	                								product_html = product_html+`
	                								<div class="col-xl-3 col-6 col-grid-box">
	                                                	<div class="product-box">
	                                                    	<div class="img-wrapper">
	                                                        	<div class="front">
	                                                            	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+product[j].item_id+`">
	                                                            		<img src="`+product[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+product[j].item_name+`">
	                                                            	</a>
	                                                        	</div>
	                                                        	<div class="back">
	                                                            	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+product[j].item_id+`">
		                                                            	<img src="`+product[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+product[j].item_name+`">
	                                                            	</a>
	                                                        	</div>
	                                                        	<div class="cart-info cart-wrap">
	                                                        	`+button+`
	                                                        	</div>
	                                                    	</div>
	                                                    	<div class="product-detail">
	                                                        	<div>
	                                                            	 <a href="product_view.php?product=`+product[j].item_id+`&cat=`+cat[i].cat_id+`">
															        	<h6 style="width: auto;">`+product[j].item_name+`</h6>
															        </a>
															        <h4>`+product[j].prices[0].formatted_discount_price+`</h4>
	                                                    		</div>
	                                                		</div>
	                                            		</div>
	                                        		</div>`;
                                        			}
                								}	
                							}
                					});	
								}
                			}
                		else
                			{
                			$.ajax({
									url:'<?=$url;?>loadItemByCategory',
									async: false,
									data:{cat_id:cat[i].cat_id, merchant_keys:'<?=$merchant_keys;?>', device_id:'<?=$device_id;?>', device_platform:'<?=$device_platform;?>', device_uiid:'<?=$device_uiid;?>', code_version:'<?=$code_version;?>', lang:'<?=$lang;?>', search_mode:'<?=$search_mode;?>', location_mode:'<?=$location_mode;?>',},
									dataType:'json',
									success:function(result1)
										{
										if(result1.code == 1)
											{
											var product = result1.details.data;
											for(var j = 0; j < product.length; j++)
												{
												if(cart.indexOf(product[j].item_id) == -1)
													{
													var button = `<span id="cart_button_`+product[j].item_id+`"><button title="Add to cart" onclick="add_to_cart(`+product[j].item_id+`,`+product[j].prices[0].discount_price+`)"> <i class="ti-shopping-cart"></i></button></span>`;
													}
												else
													{
													var button = `<a href="cart.php" title="Go To Cart"><i class="ti-shopping-cart" style="color:red;"></i></a>`;
													}
												if(favorite.indexOf(product[j].item_id) == -1)
													{
		            								button = button+`<span id="favorite_button_`+product[j].item_id+`"><button onclick="add_to_fav(`+cat[i].cat_id+`,`+product[j].item_id+`)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button></span>`;
		            								}
		            							else
		            								{
		            								button = button+`<span id="favorite_button_`+product[j].item_id+`"><button onclick="remove_fav(`+product[j].item_id+`,`+cat[i].cat_id+`)" title="Remove from Wishlist"><i class="ti-heart" aria-hidden="true"  style="color:red;" ></i></button></span>`;
		            								}
		            							button = button+`<a href="product_view.php?product=`+product[j].item_id+`&cat=`+cat[i].cat_id+`" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>`;
                								product_html = product_html+`
                								<div class="col-xl-3 col-6 col-grid-box">
                                                	<div class="product-box">
                                                    	<div class="img-wrapper">
                                                        	<div class="front">
                                                            	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+product[j].item_id+`">
                                                            		<img src="`+product[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+product[j].item_name+`">
                                                            	</a>
                                                        	</div>
                                                        	<div class="back">
                                                            	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+product[j].item_id+`">
	                                                            	<img src="`+product[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+product[j].item_name+`">
                                                            	</a>
                                                        	</div>
                                                        	<div class="cart-info cart-wrap">
                                                        	`+button+`
                                                        	</div>
                                                    	</div>
                                                    	<div class="product-detail">
                                                        	<div>
                                                            	 <a href="product_view.php?product=`+product[j].item_id+`&cat=`+cat[i].cat_id+`">
														        	<h6 style="width: auto;">`+product[j].item_name+`</h6>
														        </a>
														        <h4>`+product[j].prices[0].formatted_discount_price+`</h4>
                                                    		</div>
                                                		</div>
                                            		</div>
                                        		</div>`;
                                    			}
            								}	
            							}
            					});	
                			}
                		html_category = html_category+`<div class="custom-control custom-checkbox collection-filter-checkbox"><input type="checkbox" class="custom-control-input" `+checked+` id="cat_check_`+cat[i].cat_id+`" onclick="set_cat(`+cat[i].cat_id+`)"><label class="custom-control-label" for="cat_check_`+cat[i].cat_id+`">`+cat[i].category_name+`</label></div>`;
                    	}
                    $('#categories').html('');
                    $('#categories').html(html_category);
                    $('#products').html('');
                    $('#products').html(product_html);
                    }
                }
           	});
	});
// set category 
function set_cat(cat_id)
	{
	$.ajax({
			url:'set_category.php',
			data:{cat:cat_id},
			dataType:'json',
			success:function(result)
				{
				if(result == 1)
					{
					window.location.reload();
					}
				}
		});
	}
</script>
<?php include('footer.php'); ?>