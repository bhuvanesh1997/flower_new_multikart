<?php 
include('header.php');
if(isset($_SESSION['loggedin_user']))
{
?>
<div class="breadcrumb-section">
    <div class="container">
    	<div class="row">
        	<div class="col-sm-6">
            	<div class="page-title">
                	<h2>cart</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                	<ol class="breadcrumb">
                    	<li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">cart</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<section class="cart-section section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <table class="table cart-table table-responsive-xs">
                    <thead>
                        <tr class="table-head">
                            <th scope="col">image</th>
                            <th scope="col">product name</th>
                            <th scope="col">price</th>
                            <th scope="col">quantity</th>
                            <th scope="col">action</th>
                            <th scope="col">total</th>
                        </tr>
                    </thead>
                    <tbody id="cart_content">
                        
                    </tbody>
               	</table>
                <table class="table cart-table table-responsive-md">
                    <tfoot>
                        <tr id="cart_price_content">
                            <td>total price :</td>
                            <td>
                                <h2>$6935.00</h2>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="row cart-buttons" id="cart_button">
            <div class="col-6"><a href="#" class="btn btn-solid">continue shopping</a></div>
            <div class="col-6"><a href="#" class="btn btn-solid">check out</a></div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(window).on('load', function () 
    {
    // load cart contents
    var html_cart_product = ``;
    $.ajax({
		url:'<?=$url;?>loadCategory',
		async: false,
		data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>'},
		dataType:'json',
		success:function(result)
			{
			if(result.code == 1)
				{
				var cat = result.details.data;
				for (var i = 0; i < cat.length; i++) 
					{
					$.ajax({
							url:'<?=$url;?>loadItemByCategory',
							async: false,
							data:{cat_id:cat[i].cat_id,merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',},
							dataType:'json',
							success:function(result1)
								{
								if(result1.code == 1)
									{
									var items = result1.details.data;
									for (var j = 0; j < items.length; j++) 
										{
										if(cart.indexOf(items[j].item_id) != -1)
											{
											var qty = get_qty(items[j].item_id);
											var amount = get_amount(items[j].item_id,qty);
											html_cart_product = html_cart_product+`
<tr>
	<td>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
    		<img src="`+items[j].photo_url+`" alt="`+items[j].item_name+`">
    	</a>
    </td>
    <td>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">`+items[j].item_name+`</a>
        <div class="mobile-cart-content row">
        	<div class="col-xs-3">
            	<div class="qty-box">
                	<div class="input-group">
                    	<input type="hidden" name="item[]" value="`+items[j].item_id+`">
						<input type="number" id="qty_`+items[j].item_id+`" min="1" class="input-mini" value="`+qty+`" onchange="change_qty(`+items[j].item_id+`,`+items[j].prices[0].price+`,this.value)">
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
            	<h2 class="td-color">`+items[j].prices[0].formatted_price+`</h2>
            </div>
            <div class="col-xs-3">
            	<h2 class="td-color"><a onclick="remove_cart_item()" class="icon"><i class="ti-close"></i></a></h2>
            </div>
        </div>
    </td>
    <td>
    	<h2><i class="fa fa-inr" aria-hidden="true"></i>`+parseFloat(amount)+`/h2>
    </td>
    <td>
    	<div class="qty-box">
        	<div class="input-group">
            	<input type="number" name="quantity" class="form-control input-number" value="1">
            </div>
        </div>
    </td>
    <td>
    	<a href="#" class="icon"><i class="ti-close"></i></a>
    </td>
    <td>
    	<h2 class="td-color"><i class="fa fa-inr" aria-hidden="true"></i>`+parseFloat(amount)+`</h2>
   	</td>
</tr>`;
											}
										}
									}
								}
							});
					}
				/*html = html+`<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><strong>$`+parseFloat(total).toFixed(2)+`</strong></td></tr>`;
				$('#cart_items').html('');
				$('#cart_items').html(html);
				var html1 = `<strong>Sub-Total</strong>:$ `+parseFloat(total).toFixed(2)+`<br><strong>Total</strong>: $ `+parseFloat(total).toFixed(2)+`<br>`;
				$('#total_prices').html();
				$('#total_prices').html(html1);*/
				}
			}
		});
    $('#cart_content').html('');
    $('#cart_content').html(html_cart_product);
    });
</script>
<?php
}
else
{
echo ("<script>location.href = 'index.php';</script>");
}
include('footer.php');
?>