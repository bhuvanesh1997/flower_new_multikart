<?php include('header.php'); ?>
<style>
.mySlides {display:none;}
.w3-content,.w3-auto{margin-left:auto;margin-right:auto}.w3-content{/*max-width:980px*/}
.w3-black,.w3-hover-black:hover{color:#fff!important;background-color:#000!important}
.w3-display-left{position:absolute;top:50%;left:0%;transform:translate(0%,-50%);-ms-transform:translate(-0%,-50%)}
.w3-display-right{position:absolute;top:50%;right:0%;transform:translate(0%,-50%);-ms-transform:translate(0%,-50%)}
.w3-section,.w3-code{margin-top:16px!important;margin-bottom:16px!important}
@media (min-width: 768px) 
	{
    /* show 3 items */
    .carouselPrograms .carousel-inner .active,
    .carouselPrograms .carousel-inner .active + .carousel-item,
    .carouselPrograms .carousel-inner .active + .carousel-item + .carousel-item 
    	{
    	display: block;
    	}
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
    .carouselPrograms .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item 
    	{
        transition: none;
    	}
    .carouselPrograms .carousel-inner .carousel-item-next,
    .carouselPrograms .carousel-inner .carousel-item-prev 
    	{
        position: relative;
        transform: translate3d(0, 0, 0);
    	}
    .carouselPrograms .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item 
    	{
        position: absolute;
        top: 0;
        right: -33.333%;
        z-index: -1;
        display: block;
        visibility: visible;
    	}
    /* left or forward direction */
    .carouselPrograms .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item 
    	{
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    	}
    /* farthest right hidden item must be abso position for animations */
    .carouselPrograms .carousel-inner .carousel-item-prev.carousel-item-right 
    	{
        position: absolute;
        top: 0;
        left: 0%;
        z-index: -1;
        display: block;
        visibility: visible;
    	}
    /* right or prev direction */
    .carouselPrograms .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
    .carouselPrograms .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item 
    	{
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    	}
	}
</style>
<section class="p-0">
    <div class="slide-1 home-slider">
		<div class="w3-content w3-section">
  			<div id="banner_sliders">
  				
  			</div>
		</div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="title1 section-t-space">
                <h4 style="width: 170px;">Featured Category</h4>
                <h2 class="title-inner1" style="width: 240px;">From Seller</h2>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div id="carouselExample" class="carouselPrograms carousel slide" data-ride="carousel" data-interval="false">
    </div>
</div>
<div class="title1 section-t-space">
    <h4 style="width:170px">Exclusive Products</h4>
    <h2 class="title-inner1" style="width:230px">Products</h2>
</div>
<section class="section-b-space p-t-0 ratio_asos">
    <div class="container">
    	<div class="row">
        	<div class="col">
            	<div class="theme-tab">
                	<ul class="tabs tab-title">
                    	<li class="current"><a href="tab-1">New Products</a></li>
                        <li class=""><a href="tab-2">Deal Products</a></li>
                    </ul>
                    <div class="tab-content-cls">
                    	<div id="tab-1" class="tab-content active default">

                        </div>
                        <div id="tab-2" class="tab-content">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
var myIndex = 0;
$(window).on('load', function () 
    {
    // for banner as slider
    getappsettings();
    // for featured category
    getfeatured_cat();
    // load products
    get_products();
	});
// get banner and slider timers
function getappsettings()
	{
    $.ajax({
            url:'<?=$url;?>getAppSettings',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    var banner = `
                    				<img class="mySlides" src="`+result.details.banner.banner1+`" style="width:100%">
                    				<img class="mySlides" src="https://themes.pixelstrap.com/multikart/front-end/../assets/images/home-banner/1.jpg" style="width:100%">
									<img class="mySlides" src="https://themes.pixelstrap.com/multikart/front-end/../assets/images/home-banner/2.jpg" style="width:100%">
									<img class="mySlides" src="https://themes.pixelstrap.com/multikart/front-end/../assets/images/home-banner/3.jpg" style="width:100%">`;
                    $('#banner_sliders').html(banner);
					carousel(result.details.homebanner_interval);
                    }
                }
        });
	}
// get featured category
function getfeatured_cat()
	{
	$.ajax({
			url:'<?=$url;?>loadCategory',
			async: false,
			data:
				{
				merchant_keys:'<?=$merchant_keys;?>',
        		device_id:'<?=$device_id;?>',
        		device_platform:'<?=$device_platform;?>',
		        device_uiid:'<?=$device_uiid;?>',
		        code_version:'<?=$code_version;?>',
		        lang:'<?=$lang;?>',
		        search_mode:'<?=$search_mode;?>',
		        location_mode:'<?=$location_mode;?>',
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var cat = result.details.data;
					var featured_category = '<div class="carousel-inner row w-100 mx-auto" role="listbox">';
					var x = 0;
					for (var i = 0; i < cat.length; i++) 
						{
						if(cat[i].featured_cat == 1)
							{
featured_category = featured_category+`
<div class="carousel-item col-md-4`;
	if(x == 0)
		featured_category = featured_category+` active`;
	else
		featured_category = featured_category;
featured_category =featured_category+`">
	<div class="panel panel-default">
    	<div class="panel-thumbnail">
        	<a href="product.php?cat=`+cat[i].cat_id+`" title="`+cat[i].category_name+`" class="thumb">
            	<img class="img-fluid mx-auto d-block" src="`+cat[i].photo_url+`" alt="`+cat[i].category_name+`">
            </a>
       	</div>
       	<h4 class="text-center">`+cat[i].category_name+`</h4>
    </div>
</div>`;
							x = x+1;
							}
						}
					if(x > 0)
						{	
featured_category = featured_category+`
</div>
<a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
   	<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
	<span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>`;
						}
					else
						{ 
						featured_category = featured_category+`</div>`;
						}
					if(x > 0)
                		{
                		$('#carouselExample').html('');
						$('#carouselExample').html(featured_category);
						}
					else
						{
						$('#carouselExample').html('');
						$('#carouselExample').html(`<h4 class="text-center">Empty</h4>`);
						}
					}
				else
                	{	
                	alert("Invalid Mercant");
                    location.reload();
                	}
                }
		});
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event)
    	{
      	event.preventDefault();
      	var content = $('.modal-body');
      	content.empty();
	    var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    	});
	}
// get the products
function get_products()
	{
	$.ajax({
			url:'<?=$url;?>loadCategory',
			async: false,
			data:
				{
				merchant_keys:'<?=$merchant_keys;?>',
		        device_id:'<?=$device_id;?>',
		        device_platform:'<?=$device_platform;?>',
		        device_uiid:'<?=$device_uiid;?>',
		        code_version:'<?=$code_version;?>',
		        lang:'<?=$lang;?>',
		        search_mode:'<?=$search_mode;?>',
		        location_mode:'<?=$location_mode;?>',
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var cat = result.details.data;
					var products = '<div class="no-slider row">';
					var deal_product = '<div class="no-slider row">';
					var x = 0;
					for (var i = 0; i < cat.length; i++) 
						{
						$.ajax({
								url:'<?=$url;?>loadItemByCategory',
								async: false,
								data:
									{
									cat_id:cat[i].cat_id,
									merchant_keys:'<?=$merchant_keys;?>',
							        device_id:'<?=$device_id;?>',
							        device_platform:'<?=$device_platform;?>',
							        device_uiid:'<?=$device_uiid;?>',
							        code_version:'<?=$code_version;?>',
							        lang:'<?=$lang;?>',
							        search_mode:'<?=$search_mode;?>',
							        location_mode:'<?=$location_mode;?>',
									},
								dataType:'json',
								success:function(result1)
									{
									if(result1.code == 1)
										{
										var items = result1.details.data;
										for (var j = 0; j < items.length; j++) 
											{
											if(cart.indexOf(items[j].item_id) == -1)
												{
												var button = `<span id="cart_button_`+items[j].item_id+`"><button title="Add to cart" onclick="add_to_cart(`+items[j].item_id+`,`+items[j].prices[0].discount_price+`)"> <i class="ti-shopping-cart"></i></button></span>`;
												}
											else
												{
												var button = `<a href="cart.php" title="Go To Cart"><i class="ti-shopping-cart" style="color:red;"></i></a>`;
												}
											if(favorite.indexOf(items[j].item_id) == -1)
												{
	            								button = button+`<span id="favorite_button_`+items[j].item_id+`"><button onclick="add_to_fav(`+cat[i].cat_id+`,`+items[j].item_id+`)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button></span>`;
	            								}
	            							else
	            								{
	            								button = button+`<span id="favorite_button_`+items[j].item_id+`"><button onclick="remove_fav(`+items[j].item_id+`,`+cat[i].cat_id+`)" title="Remove from Wishlist"><i class="ti-heart" aria-hidden="true"  style="color:red;" ></i></button></span>`;
	            								}
	            							button = button+`<a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>`;
products = products+`
<div class="product-box">
		<div class="img-wrapper">
	    	<div class="front">
	        	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
	                <img src="`+items[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+items[j].item_name+`">
				</a>
	        </div>
	       	<div class="back">
	        	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
	                <img src="`+items[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+items[j].item_name+`">
				</a>
	        </div>
	        <div class="cart-info cart-wrap" id="buttons_`+items[j].item_id+`">
	        	 `+button+`
			</div>
	    </div>
	    <div class="product-detail">
	        <a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`">
	        	<h6 style="width: auto;">`+items[j].item_name+`</h6>
	        </a>
	        <h4>`+items[j].prices[0].formatted_discount_price+`</h4>
	    </div>
	</div>`;
											if(items[j].deal_status == 1)
												{
												deal_product = deal_product+`
<div class="product-box">
	<div class="img-wrapper">
    	<div class="front">
        	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
                <img src="`+items[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+items[j].item_name+`">
			</a>
        </div>
       	<div class="back">
        	<a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
                <img src="`+items[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+items[j].item_name+`">
			</a>
        </div>
        <div class="cart-info cart-wrap" id="buttons_`+items[j].item_id+`">
        	 `+button+`
		</div>
    </div>
    <div class="product-detail">
        <a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`">
        	<h6 style="width: auto;">`+items[j].item_name+`</h6>
        </a>
        <h4>`+items[j].prices[0].formatted_discount_price+`</h4>
    </div>
</div>`;
												x = x+1;
												}
											}
										}
									}
							});
						}
					products =  products+`</div>`;
					if(x==0)
						{
						deal_product = deal_product+`<h4 class="text-center">Empty</h4>`;	
						}
					deal_product =  deal_product+`</div>`;
					$('#tab-1').html(products);
					$('#tab-2').html(deal_product);
					}
				}
			});
	}
function carousel(interval) 
	{
  	var i;
  	var x = document.getElementsByClassName("mySlides");
  	for (i = 0; i < x.length; i++) 
  		{
    	x[i].style.display = "none";  
  		}
  	myIndex++;
  	if (myIndex > x.length) 
  		{
  		myIndex = 1
  		}    
  	x[myIndex-1].style.display = "block";  
  	setTimeout(carousel, 2000); // Change image every 2 seconds
	}
$('#carouselExample').on('slide.bs.carousel', function (e) 
	{
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;
    if (idx >= totalItems-(itemsPerSlide-1)) 
    	{
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) 
        	{
            // append slides to end
            if (e.direction=="left") 
            	{
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            	}
            else
            	{
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            	}
        	}
    	}
	});
</script>
<?php include('footer.php'); ?>