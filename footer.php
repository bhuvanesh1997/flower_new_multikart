<section class="section-b-space light-layout">
    <div class="container">
    	<div class="row footer-theme partition-f">
        	<div class="col-lg-4 col-md-6">
            	<div class="footer-title footer-mobile-title">
                	<h4>About</h4>
               	</div>
                <div class="footer-contant">
                	<div class="footer-logo">
                		<img id="footer_logo">
                	</div>
                    <p id="merchant_detail"></p>
                    <div class="footer-social">
                    	<ul>
                        	<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col offset-xl-1">
            	<div class="sub-title">
                	<div class="footer-title">
                        <h4>Help</h4>
                    </div>
                    <div class="footer-contant">
                        <li><a data-toggle="modal" data-target="#login_model">Login</a></li>
                        <li><a data-toggle="modal" data-target="#register_model">Register</a></li>
                    </div>
                </div>
            </div>
            <div class="col">
            	<div class="sub-title">
                	<div class="footer-title">
                        <h4>Categories</h4>
                    </div>
                    <div class="footer-contant" id="category_footer">
                    </div>
                </div>
            </div>
            <div class="col">
            	<div class="sub-title">
                	<div class="footer-title">
                    	<h4>Our Contact</h4>
                    </div>
                    <div class="footer-contant">
                    	<ul class="contact-list">
                        	<li id="merchant_address"></li>
                            <li id="merchant_phone"></li>
                            <li id="merchant_email"></li>
                            <li id="merchant_fax"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="uk-text-small uk-text-muted uk-margin-medium-top">        
      <div>
        All rights reserved 2020  
        <a class="uk-link-muted" rel="alternate" hreflang="en" href="http://localhost/bhuvanesh/calicut/awad/en">
          English
        </a>
        .
        <a href="#" uk-totop="" uk-scroll="" class="uk-icon uk-totop"><svg width="18" height="10" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg" data-svg="totop"><polyline fill="none" stroke="#000" stroke-width="1.2" points="1 9 9 1 17 9 "></polyline></svg></a>
      </div>
    </div>
</div>
<script type="text/javascript">
$(window).on('load', function () 
    {
    // get merchant logo and basic deails
	$.ajax({
			url:'<?=$url;?>getMerchantInfo',
			async: false,
			data:
				{
				merchant_keys:'<?=$merchant_keys;?>',
		        device_id:'<?=$device_id;?>',
		        device_platform:'<?=$device_platform;?>',
		        device_uiid:'<?=$device_uiid;?>',
		        code_version:'<?=$code_version;?>',
		        lang:'<?=$lang;?>',
		        search_mode:'<?=$search_mode;?>',
		        location_mode:'<?=$location_mode;?>',
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					$('#footer_logo').attr('src',result.details.data.logo);
					$('#footer_logo').attr('style','width:179px;height:34px;');
					$('#footer_logo').attr('atr',result.details.data.merchant_name);
					$('#merchant_address').html('<i class="fa fa-map-marker"></i>'+result.details.data.address);
					$('#merchant_phone').html('<i class="fa fa-phone"></i>Call Us: '+result.details.data.contact_phone);
					$('#merchant_email').html('');
					$('#merchant_fax').html('');
					$('#merchant_detail').html(result.details.data.free_delivery);
					}
				}
		});
    // footer categories load
    $.ajax({
            url:'<?=$url;?>loadCategory',
            data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',},
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    $('#category_footer').html('');
                    var cat = result.details.data;
                    var category_footer = ""; 
                    for (var i = 0; i < cat.length; i++) 
                        {
                        category_footer = category_footer+`<li><a href="list_page.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
                        }
                    $('#category_footer').html(category_footer);
                    }
                }
            });
	});
// add to cart
function add_to_cart(item_id,price)
    {
    if(token == '')
        {
        Swal.fire({
                icon: "error",
                title: "Failed!",
                text: 'Please Login to continue!',
                timer: 3000,
                showConfirmButton: false,
                });
        return;
        }
    $.ajax({
            url:'<?=$url;?>addToCart?&merchant_keys=<?=$merchant_keys;?>&device_id=<?=$device_id;?>&device_platform=<?=$device_platform;?>&device_uiid=<?=$device_uiid;?>&code_version=<?=$code_version;?>&lang=<?=$lang;?>&search_mode=<?=$search_mode;?>&location_mode=<?=$location_mode;?>',
            method:'post',
            data:
                {
                item_id:parseInt(item_id),
                token:token,
                qty:1,
                price:parseFloat(price),
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 0)
                    {
                    Swal.fire({
                                icon: "error",
                                title: "Failed!",
                                text: result.msg,
                                timer: 3000,
                                showConfirmButton: false,
                                });
                    }
                else if(result.code == 1)
                    {
                    Swal.fire({
                                icon: "success",
                                title: "Success",
                                text: 'Item added to cart.',
                                timer: 3000,
                                showConfirmButton: false,
                                });
                    $('#cart_button_'+item_id).html(``);
                    $('#cart_button_'+item_id).html(`<a href="cart.php" title="Go To Cart"><i class="ti-shopping-cart" style="color:red;"></i></a>`);
                    /*$("#cart_item").load(location.href+" #cart_item>*","");
                    $("#cart_count").load(location.href+" #cart_count>*","");*/
                    /*$('#cart_item').load('#cart_item');
                    $('#cart_count').load('#cart_count')*/
                    }
                }
            });
    }
// add o favorite wish list
function add_to_fav(cat_id,item_id)
    {
    if(token == '')
        {
        Swal.fire({
                icon: "error",
                title: "Failed!",
                text: 'Please Login to continue!',
                timer: 3000,
                showConfirmButton: false,
                });
        return;
        }
    $.ajax({
            url:'<?=$url;?>AddFavorite',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                item_id:parseInt(item_id),
                category_id:parseInt(cat_id),
                token:token,
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 0)
                    {
                    Swal.fire({
                                icon: "error",
                                title: "Failed!",
                                text: result.msg,
                                timer: 3000,
                                showConfirmButton: false,
                                });
                    }
                else if(result.code == 1)
                    {
                    Swal.fire({
                                icon: "success",
                                title: "Success",
                                text: 'Item added to your Wishlist.',
                                timer: 3000,
                                showConfirmButton: false,
                                });
                    $('#favorite_button_'+item_id).html(``);
                    $('#favorite_button_'+item_id).html(`<button onclick="remove_fav(`+item_id+`)" title="Remove from Wishlist"><i class="ti-heart" aria-hidden="true"  style="color:red;" ></i></button>`);
                    /*$('#tab-1').load('#tab-1');
                    $('#tab-2').load('#tab-2');*/
                    // window.location.reload();
                    }
                }
            });
    }
function remove_fav(item_id,cat_id)
    {
    if(token == '')
        {
        Swal.fire({
                type: "error",
                title: "Failed!",
                text: 'Please SignIn to continue!',
                timer: 3000,
                showConfirmButton: false,
                });
        return;
        }
    $.ajax({
            url:'<?=$url;?>RemoveFavorite',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                item_id:item_id,
                token:token,
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 0)
                    {
                    Swal.fire({
                                icon: "error",
                                title: "Failed!",
                                text: result.msg,
                                timer: 3000,
                                showConfirmButton: false,
                                });
                    }
                else if(result.code == 1)
                    {
                    Swal.fire({
                                icon: "success",
                                title: "Success",
                                text: 'Item removed from your Wishlist.',
                                timer: 3000,
                                showConfirmButton: false,
                                });
                    $('#favorite_button_'+item_id).html(``);
                    $('#favorite_button_'+item_id).html(`<button onclick="add_to_fav(`+cat_id+`,`+item_id+`)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button>`);
                    }
                }
            });
    }
// get quantity from cart
function get_qty(item_id)
    {
    for (var i = 0; i < cart_contents[0].length; i++)
        {
        if(cart_contents[0][i].item_id == item_id)
            {
            return cart_contents[0][i].qty;
            }
        }
    }
// get cart amount based on qty
function get_amount(item_id,qty)
    {
    for (var i = 0; i < cart_contents.length; i++)
        {
        if(cart_contents[i].item_id == item_id)
            {
            var amount = parseFloat(cart_contents[i].discounted_price) * parseFloat(qty);
            return parseFloat(amount);
            }
        }
    }

</script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/jquery.exitintent.js"></script>
<script src="assets/js/exit.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/slick.js"></script>
<script src="assets/js/menu.js"></script>
<script src="assets/js/lazysizes.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/bootstrap-notify.min.js"></script>
<script src="assets/js/script.js"></script>
<script src="assets/js/jquery.elevatezoom.js"></script>