<?php
if(isset($_GET['cat']))
{
if(isset($_GET['product']))
{
include('header.php');
?>
<div class="breadcrumb-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="page-title">
                    <h2>product</h2>
                </div>
            </div>
            <div class="col-sm-6">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">product</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<section>
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-slick" id="product_image">
                    </div>
                    <div class="row" style="padding-top: 10px">
                        <div class="col-12 p-0">
                            <div class="slider-nav" id="product_image1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 rtl-text">
                    <div class="product-right">
                        <h2 class="mb-0" id="product_name">Women Pink Shirt</h2>
                        <h5 class="mb-2" id="product_category">by <a href="#">zara</a></h5>
                        <h4 id="product_discount"><del>$459.00</del><span>55% off</span></h4>
                        <h3 id="product_price">$32.96</h3>
                        <form id="add_to_cart">
                            <div class="product-description border-product">
                                <h6 class="product-title">quantity</h6>
                                <div class="qty-box">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <button type="button" class="btn quantity-left-minus" data-type="minus" data-field=""><i class="ti-angle-left"></i></button> 
                                        </span>
                                        <input type="text" name="quantity" class="form-control input-number" value="1">
                                        <span class="input-group-prepend">
                                            <button type="button" class="btn quantity-right-plus" data-type="plus" data-field=""><i class="ti-angle-right"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="product-buttons" id="product_cart">
                                <a onclick="add_to_cart()" class="btn btn-solid">add to cart</a> 
                            </div>
                        </form>
                        <div class="border-product">
                            <h6 class="product-title">product details</h6>
                            <p id="product_detail">Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem,</p>
                        </div>
                        <div class="border-product">
                            <div class="product-icon" id="product_wishlist">
                                <button class="wishlist-btn"><i class="fa fa-heart"></i><span class="title-font">Add To WishList</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-b-space ratio_asos">
    <div class="container">
        <div class="row">
            <div class="col-12 product-related">
                <h2>related products</h2>
            </div>
        </div>
        <div class="row search-product" id="product_related">
            <div class="col-xl-2 col-md-4 col-sm-6">
                <div class="product-box">
                    <div class="img-wrapper">
                        <div class="front">
                            <a href="#"><img src="https://themes.pixelstrap.com/multikart/front-end/../assets/images/pro3/33.jpg" class="img-fluid blur-up lazyload bg-img" alt=""></a>
                        </div>
                        <div class="back">
                            <a href="#"><img src="https://themes.pixelstrap.com/multikart/front-end/../assets/images/pro3/34.jpg" class="img-fluid blur-up lazyload bg-img" alt=""></a>
                        </div>
                        <div class="cart-info cart-wrap">
                            <button data-toggle="modal" data-target="#addtocart" title="Add to cart"><i class="ti-shopping-cart"></i></button> <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></a> <a href="#" data-toggle="modal" data-target="#quick-view" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="product-detail">
                        <a href="product-page(no-sidebar).html">
                            <h6>Slim Fit Cotton Shirt</h6>
                        </a>
                        <h4>$500.00</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function()
    {
    $.ajax({
            url:'<?=$url;?>loadCategory',
            async: false,
            data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',},
            dataType:'json',
            success:function(result)
                {
                var related_product = ``;
                if(result.code == 1)
                    {
                    var cat = result.details.data;
                    for (var i = 0; i < cat.length; i++) 
                        {
                        if(cat[i].cat_id == '<?=$_GET['cat'];?>')
                            {
                            $('#product_category').html('');
                            $('#product_category').html(`by <a href="list_page.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></h5>`);
                            $.ajax({
                                    url:'<?=$url;?>loadItemDetails',
                                    async: false,
                                    data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',cat_id:'<?=$_GET["cat"];?>',item_id:'<?=$_GET["product"];?>'},
                                    dataType:'json',
                                    success:function(result1)
                                        {
                                        if(result1.code == 1)
                                            {
                                            var product = result1.details.data;
                                            $('#product_image').html('');
                                            $('#product_image').html(`<div><img style="width:450px;height:612px;" src="`+product.photo+`" alt="`+product.item_name+`" class="img-fluid blur-up lazyload image_zoom_cls-0"></div>`);
                                            $('#product_image1').html('');
                                            $('#product_image1').html(`<div><img  style="width:128px;height:174px;" src="`+product.photo+`" alt="`+product.item_name+`" class="img-fluid blur-up lazyload"></div>`);
                                            $('#product_name').html('');
                                            $('#product_name').html(product.item_name);
                                            var cost = product.prices[0].price;
                                            var sell = product.prices[0].discount_price;
                                            var discount = cost - sell;
                                            var product_discount = (discount/cost)*100;
                                            var product_discount = Math.round(product_discount);
                                            $('#product_discount').html('');
                                            $('#product_discount').html(`<del>`+product.prices[0].formatted_price+`</del><span>`+product_discount+`% off</span>`);
                                            $('#product_price').html('');
                                            $('#product_price').html(product.prices[0].formatted_discount_price);
                                            if(cart.indexOf(product.item_id) == -1)
                                                { 
                                                $('#product_cart').html(`<a onclick="add_to_cart(`+product.item_id+`,`+product.prices[0].price+`)" class="btn btn-solid">Add to Cart</a>`);
                                                }
                                            else
                                                {
                                                $('#add_to_cart').html('');
                                                $('#add_to_cart').html(`<a href="cart.php" class="btn btn-solid">Go to Cart</a>`);
                                                }   
                                            $('#product_detail').html('');
                                            $('#product_detail').html(product.item_description);
                                            $('#product_wishlist').html('');
                                            if(favorite.indexOf(product.item_id) == -1)
                                                {
                                                $('#product_wishlist').html(`<button class="wishlist-btn" onclick="add_to_fav(`+product.item_id+`,`+cat[i].cat_id+`)"><i class="fa fa-heart"></i><span class="title-font">Add To WishList</span></button>`);
                                                }
                                            else
                                                {
                                                $('#product_wishlist').html(`<button class="wishlist-btn" onclick="remove_fav(`+product.item_id+`)"><i class="fa fa-heart" style="color:red"></i><span class="title-font">Remove Wishlist</span></button>`);
                                                }
                                            }
                                        }
                                });
                            $.ajax({
                                    url:'<?=$url;?>loadItemByCategory',
                                    async: false,
                                    dataType:'json',
                                    data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',cat_id:'<?=$_GET["cat"];?>'},
                                    success:function(result2)
                                        {
                                        if(result2.code == 1)
                                            {
                                            var items = result2.details.data;
                                            for (var j = 0; j < items.length; j++) 
                                                {
                                                if(cart.indexOf(items[j].item_id) == -1)
                                                    {
                                                    var button = `<span id="cart_button_`+items[j].item_id+`"><button title="Add to cart" onclick="add_to_cart(`+items[j].item_id+`,`+items[j].prices[0].discount_price+`)"> <i class="ti-shopping-cart"></i></button></span>`;
                                                    }
                                                else
                                                    {
                                                    var button = `<a href="cart.php" title="Go To Cart"><i class="ti-shopping-cart" style="color:red;"></i></a>`;
                                                    }
                                                if(favorite.indexOf(items[j].item_id) == -1)
                                                    {
                                                    button = button+`<span id="favorite_button_`+items[j].item_id+`"><button onclick="add_to_fav(`+cat[i].cat_id+`,`+items[j].item_id+`)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></button></span>`;
                                                    }
                                                else
                                                    {
                                                    button = button+`<span id="favorite_button_`+items[j].item_id+`"><button onclick="remove_fav(`+items[j].item_id+`,`+cat[i].cat_id+`)" title="Remove from Wishlist"><i class="ti-heart" aria-hidden="true"  style="color:red;" ></i></button></span>`;
                                                    }
                                                button = button+`<a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`" title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>`;
                                                related_product = related_product+`
<div class="col-xl-2 col-md-4 col-sm-6">
    <div class="product-box">
        <div class="img-wrapper">
            <div class="front">
                <a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
                    <img src="`+items[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+items[j].item_name+`">
                </a>
            </div>
            <div class="back">
                <a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
                    <img src="`+items[j].photo_url+`" class="img-fluid blur-up lazyload bg-img" alt="`+items[j].item_name+`">
                </a>
            </div>
            <div class="cart-info cart-wrap" id="buttons_`+items[j].item_id+`">
                 `+button+`
            </div>
        </div>
        <div class="product-detail">
            <a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`">
                <h6 style="width: auto;">`+items[j].item_name+`</h6>
            </a>
            <h4>`+items[j].prices[0].formatted_discount_price+`</h4>
        </div>
    </div>
</div>`;
                                                }
                                            }
                                        }
                                    });
                            }
                        
                        }
                    $('#product_related').html('');
                    $('#product_related').html(related_product);
                    }
                }
          });
      })
</script>
<?php
include('footer.php');
}
else
{
header('Location: index.php');
}
}
else
{
header('Location: index.php');
}
?>