<?php 
include('variable.php'); 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="multikart">
    <meta name="keywords" content="multikart">
    <meta name="author" content="multikart">
    <link rel="icon" id="icon" type="image/x-icon">
    <link rel="shortcut icon" id="icon1" type="image/x-icon">
    <title id="title"></title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/color1.css" media="screen" id="color">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <style type="text/css">
        #cart_count
            {
            border-radius: 50%;
            font-size: 15px;
            color: white;
            text-align: center;
            background: red;
            }
    </style>
</head>
<body>
<div id="ColorPicker1"></div>
<div class="modal fade" id="login_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Log in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-header text-center" id="login_form_msg" style="display: none;">
                <span id="login_form_msg_error" style="color: red"></span>
            </div>
            <form id="login_form" onsubmit="return false">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-envelope-o prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
                        <input type="email" id="login_email" name="login_email" class="form-control validate" required="" value="">
                    </div>
                    <div class="md-form mb-4">
                        <i class="fa fa-key prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
                        <input type="password" id="login_password" name="login_password" class="form-control validate" required="">
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default" onclick="login_form()">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="register_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Register</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-header text-center" id="register_form_msg" style="display: none;">
                <span id="register_form_msg_error" style="color: red"></span>
            </div>
            <form id="register_form" onsubmit="return false">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-address-card-o prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-fisrt_name">First Name</label>
                        <input placeholder="First Name" id="register_name" type="text" class="form-control " name="register_name" required="" onkeypress="return /[a-z]/i.test(event.key)">
                    </div>
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-address-card-o prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-last_name">Last Name</label>
                        <input placeholder="Last Name" id="register_lname" type="text" class="form-control " name="register_lname" required="" onkeypress="return /[a-z]/i.test(event.key)">
                    </div>
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-mobile prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-mobile">Phone Number</label>
                        <input placeholder="Phone Number" id="register_phone" type="text" class="form-control " name="register_phone" value="" required="" onkeypress="return /[0-9]/i.test(event.key)" maxlength="10">
                    </div>
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-envelope prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-email">Email</label>
                        <input placeholder="Email Id" id="register_email" type="email" class="form-control " name="register_email" required="">
                    </div>
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-unlock-alt prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-password">Password</label>
                        <input placeholder="Password" id="register_password" type="password" class="form-control " name="register_password" required="">
                    </div>
                    <div class="md-form mb-5" style="margin-bottom: 1rem !important;">
                        <i class="fa fa-unlock-alt prefix grey-text"></i> <label data-error="wrong" data-success="right" for="defaultForm-confirm_password">Confirm Password</label>
                        <input placeholder="Confirm Password" id="confirm_password" type="password" class="form-control" name="confirm_password" required="">
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-default" onclick="register_form()">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="loader_skeleton">
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="header-contact">
                        <ul>
                            <li id="shop_name">Welcome to Our store Multikart</li>
                            <li id="shop_contact"><i class="fa fa-phone" aria-hidden="true"></i>Call Us: 123 - 456 - 7890</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <ul class="header-dropdown">
                        <?php 
                        if(isset($_SESSION['loggedin_user']))
                            {
                            ?>
                            <li class="mobile-wishlist"><a href="favorite.php"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                            <?php 
                            }
                        ?>
                        <li class="onhover-dropdown mobile-account"> <i class="fa fa-user" aria-hidden="true"></i>
                                My Account
                            <ul class="onhover-show-div">
                                <?php 
                                if(isset($_SESSION['loggedin_user']))
                                    {
                                    ?>
                                    <li><a href="logout.php">Logout</a></li>
                                    <?php
                                    }
                                else
                                    {   
                                    ?>
                                    <li><a data-toggle="modal" data-target="#login_model">Login</a></li>
                                    <li><a data-toggle="modal" data-target="#register_model">Register</a></li>
                                    <?php 
                                    }
                                ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-menu">
                        <div class="menu-left">
                            <div class="navbar">
                                <a href="javascript:void(0)" onclick="openNav()">
                                    <div class="bar-style"><i class="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                                    </div>
                                </a>
                                <div id="mySidenav" class="sidenav">
                                    <a href="javascript:void(0)" class="sidebar-overlay" onclick="closeNav()"></a>
                                    <nav>
                                        <div onclick="closeNav()">
                                            <div class="sidebar-back text-left"><i class="fa fa-angle-left pr-2" aria-hidden="true"></i> Back</div>
                                        </div>
                                        <ul id="sub-menu" class="sm pixelstrap sm-vertical">
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="brand-logo">
                                <a href="index.php"><img id="merchant_logo" alt="asdasd" class="img-fluid blur-up lazyloaded" alt=""></a>
                            </div>
                        </div>
                        <div class="menu-right pull-right">
                            <div>
                                <div class="icon-nav">
                                    <ul>
                                        <li class="onhover-div mobile-search">
                                            <div>
                                                <img src="assets/images/icon/search.png" onclick="openSearch()" class="img-fluid blur-up lazyloaded" alt=""> <i class="ti-search" onclick="openSearch()"></i>
                                            </div>
                                            <div id="search-overlay" class="search-overlay">
                                                <div> 
                                                    <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
                                                    <div class="overlay-content">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-xl-12">
                                                                    <form>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Search a Product">
                                                                        </div>
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php 
                                        if(isset($_SESSION['loggedin_user']))
                                            {
                                            ?>
                                            <li class="onhover-div mobile-cart">
                                                <div>
                                                    <img src="assets/images/icon/cart.png" class="img-fluid blur-up lazyloaded" alt=""> <i class="ti-shopping-cart"></i><sup id="cart_count"></sup>
                                                </div>
                                                <ul class="show-div shopping-cart" id="cart_item">
                                                </ul>
                                            </li>
                                            <?php 
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script>
var favorite = [];
var cart = [];
var cart_contents = [];
var token = '';
var total = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
    {
    ?>
    token = "<?=$_SESSION['loggedin_user']['token'];?>";
    // get favorite list if logged in
    $.ajax({
            url:'<?=$url;?>ItemFavoritesList',
            async: false,
            data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',token:token},
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    var data = result.details.data;
                    if(data.length > 0)
                        {
                        for (var i = 0;i < data.length; i++)
                            {
                            favorite.push(data[i].item_id);
                            }
                        }
                    }
                }
        });
    // get the items in cart if logged in
    $.ajax({
            url:'<?=$url;?>loadCart',
            async: false,
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',transaction_type:'delivery',
                token:token,
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    total = result.details.cart_details.cart_subtotal;
                    var data = result.details.data.item;
                    cart_contents.push(data);
                    var array = [];
                    var array = Object.values(data);
                    if(array.length > 0)
                        {
                        for (var i = 0;i < array.length; i++)
                            {
                            cart.push(array[i].item_id);
                            }
                        }
                    }
                }
        });
    <?php 
    }
?>
$(window).on('load', function () 
    {
    // get merchant basic details and logos
    $.ajax({
            url:'<?=$url;?>getMerchantInfo',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    $('#icon').attr('href',result.details.data.logo);
                    $('#icon1').attr('href',result.details.data.logo);
                    $('#icon').attr('style','height:32px;width:32px');
                    $('#icon1').attr('style','height:32px;width:32px');
                    $('#merchant_logo').attr('src',result.details.data.logo);
                    $('#merchant_logo').attr('atr',result.details.data.merchant_name);
                    $('#merchant_logo').attr('style','width:179px;height:34px');
                    $('#merchant_logo1').attr('src',result.details.data.logo);
                    $('#merchant_logo1').attr('atr',result.details.data.merchant_name);
                    $('#merchant_logo1').attr('style','width:179px;height:34px');
                    $('#title').html(result.details.data.merchant_name);
                    $('#shop_name').html('');
                    $('#shop_name').html(result.details.data.merchant_name);
                    $('#shop_contact').html('');
                    $('#shop_contact').html('<i class="fa fa-phone" aria-hidden="true"></i> Call Us: '+result.details.data.contact_phone);
                    $('#shop_name1').html('');
                    $('#shop_name1').html(result.details.data.merchant_name);
                    $('#shop_contact1').html('<i class="fa fa-phone" aria-hidden="true"></i> Call Us: '+result.details.data.contact_phone);
                    $('#sub-menu1').html('');
                    $('#sub-menu').html("");
                    $.ajax({
                            url:'<?=$url;?>loadCategory',
                            data:
                                {
                                merchant_keys:'<?=$merchant_keys;?>',
                                device_id:'<?=$device_id;?>',
                                device_platform:'<?=$device_platform;?>',
                                device_uiid:'<?=$device_uiid;?>',
                                code_version:'<?=$code_version;?>',
                                lang:'<?=$lang;?>',
                                search_mode:'<?=$search_mode;?>',
                                location_mode:'<?=$location_mode;?>',
                                },
                              dataType:'json',
                              success:function(result)
                                {
                                if(result.code == 1)
                                    {
                                    var cat = result.details.data;
                                    var html = ""; 
                                    for (var i = 0; i < cat.length; i++) 
                                        {
                                        html = html+`<li><a href="list_page.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
                                        }
                                    $('#sub-menu').html(html);
                                    }
                                else
                                    {
                                    $('#sub-menu').html('<li>Empty</li>');
                                    }
                                }
                        });
                    }
                else
                    {
                    alert("Invalid Mercant");
                    location.reload();
                    }
                }
        });
    // get cart count and card contents
    $.ajax({
            url:'<?=$url;?>getCartCount',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                token:token,
                },
            dataType:'json',
            async: false,
            success:function(result)
                {
                if(result.code == 1)
                    {
                    $('#cart_count').html('');
                    $('#cart_count').html(result.details.count);
                    }
                }
            });
    // get cart products
    $.ajax({
            url:'<?=$url;?>loadCategory',
            async: false,
            data:{merchant_keys:'<?=$merchant_keys;?>',device_id:'<?=$device_id;?>',device_platform:'<?=$device_platform;?>',device_uiid:'<?=$device_uiid;?>',code_version:'<?=$code_version;?>',lang:'<?=$lang;?>',search_mode:'<?=$search_mode;?>',location_mode:'<?=$location_mode;?>',},
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    var cat = result.details.data;
                    var cart_product = ``;
                    for (var i = 0; i < cat.length; i++) 
                        {
                        $.ajax({
                                url:'<?=$url;?>loadItemByCategory',
                                async: false,
                                data:
                                    {
                                    cat_id:cat[i].cat_id,
                                    merchant_keys:'<?=$merchant_keys;?>',
                                    device_id:'<?=$device_id;?>',
                                    device_platform:'<?=$device_platform;?>',
                                    device_uiid:'<?=$device_uiid;?>',
                                    code_version:'<?=$code_version;?>',
                                    lang:'<?=$lang;?>',
                                    search_mode:'<?=$search_mode;?>',
                                    location_mode:'<?=$location_mode;?>',
                                    },
                                dataType:'json',
                                success:function(result1)
                                    {
                                    if(result1.code == 1)
                                        {
                                        var items = result1.details.data;
                                        for (var j = 0; j < items.length; j++) 
                                            {
                                            if(cart.indexOf(items[j].item_id) != -1)
                                                {
                                                var row = 0;
                                                $.each( cart_contents[0], function( key, value ) 
                                                    {
                                                    if(value.item_id == items[j].item_id)
                                                        {
                                                        var row = key;
                                                        }
                                                    });    
                                                var qty = get_qty(items[j].item_id);
                                                var amount = get_amount(items[j].item_id,qty);
                                                cart_product = cart_product+`
                                                <li>
                                                    <div class="media">
                                                        <a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
                                                            <img class="mr-3" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`">
                                                        </a>
                                                        <div class="media-body">
                                                            <a href="products.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
                                                                <h4>`+items[j].item_name+`</h4>
                                                            </a>
                                                            <h4>
                                                                <span>`+qty+` x `+items[j].prices[0].formatted_discount_price+`</span>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="close-circle">
                                                        <a href="cart.php"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                    </div>
                                                </li>
                                                `;
                                                }
                                            }
                                        }
                                    }
                            });
                        }
                    cart_product = cart_product+`<li>
                                                    <div class="total">
                                                        <h5>subtotal : <span><i class="fa fa-inr" aria-hidden="true"></i>`+parseFloat(total).toFixed(2)+`</span></h5>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="buttons">
                                                        <a href="cart.php" class="view-cart">view cart</a> 
                                                        <a href="checkout.php" class="checkout">checkout</a>
                                                    </div>
                                                </li>`;
                    $('#cart_item').html(cart_product);
                    }
                }
            });

    });
// for search to come
function openSearch() 
    {
    document.getElementById("search-overlay").style.display = "block";
    }
// for search o close
function closeSearch() 
    {
    document.getElementById("search-overlay").style.display = "none";
    }
// login function to check login
function login_form()
    {
    var email = document.forms["login_form"]["login_email"];               
    var password = document.forms["login_form"]["login_password"];
    if (email.value == "")                               
        {
        $('#login_form_msg').show();
        $('#login_form_msg_error').html('');
        $('#login_form_msg_error').html("Please enter your email id.");
        return false; 
        } 
    if (password.value == "")                                   
        {
        $('#login_form_msg').show();
        $('#login_form_msg_error').html('');
        $('#login_form_msg_error').html("Please enter your password.");
        return false;  
        } 
    $.ajax({
            url:'<?=$url;?>login',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                username:email.value,
                password:password.value
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    $('#login_form_msg').show();
                    $('#login_form_msg_error').html('');
                    $('#login_form_msg_error').html(result.msg);
                    document.getElementById("login_form").reset();
                    $.ajax({
                            url:'<?=$url;?>getUserProfile',
                            data:
                                {
                                merchant_keys:'<?=$merchant_keys;?>',
                                device_id:'<?=$device_id;?>',
                                device_platform:'<?=$device_platform;?>',
                                device_uiid:'<?=$device_uiid;?>',
                                code_version:'<?=$code_version;?>',
                                lang:'<?=$lang;?>',
                                search_mode:'<?=$search_mode;?>',
                                location_mode:'<?=$location_mode;?>',
                                token:result.details.token,
                                },
                            dataType:'json',
                            success:function(result1) 
                                {
                                if(result1.code == 1)
                                    { 
                                    $.ajax({
                                            url:'set_session.php',
                                            data:
                                                {
                                                'data':result1.details.data,
                                                'token':result.details.token,
                                                },
                                            dataType:'json',
                                            success:function(result1) 
                                                {
                                                window.location = "index.php";
                                                },
                                        });
                                    }
                                else
                                    {
                                    window.location.reload;
                                    }
                                }
                        });
                    }
                else
                    {
                    $('#login_form_msg').show();
                    $('#login_form_msg_error').html('');
                    $('#login_form_msg_error').html(result.msg);
                    return false;
                    }
                }
        });
    }
// register customer
function register_form()
    {
    var name = document.forms["register_form"]["register_name"];            
    var lname = document.forms["register_form"]["register_lname"];               
    var phone = document.forms["register_form"]["register_phone"];               
    var email = document.forms["register_form"]["register_email"];               
    var password = document.forms["register_form"]["register_password"];  
    var password_confirmation =  document.forms["register_form"]["confirm_password"];  
    if (name.value == "")                                  
        {
        $('#register_form_msg').show();
        $('#register_form_msg_error').html('');
        $('#msg').html("Please enter your name.");
        return false; 
        } 
    if (phone.value == "")                               
        {
        $('#register_form_msg').show();
        $('#register_form_msg_error').html('');
        $('#register_form_msg_error').html("Please enter your Contact Number.");
        return false; 
        } 
    if (email.value == "")                               
        {
        $('#register_form_msg').show();
        $('#register_form_msg_error').html('');
        $('#register_form_msg_error').html("Please enter your email id.");
        return false; 
        } 
    if (password.value == "")                                   
        {
        $('#register_form_msg').show();
        $('#register_form_msg_error').html('');
        $('#register_form_msg_error').html("Please enter your password.");
        return false;  
        } 
    if (password_confirmation.value == "")                                   
        {
        $('#register_form_msg').show();
        $('#register_form_msg_error').html('');
        $('#register_form_msg_error').html("Please enter your confirm password.");
        return false;  
        } 
    if (password.value != password_confirmation.value)
        {
        $('#register_form_msg').show();
        $('#register_form_msg_error').html('');
        $('#register_form_msg_error').html("Password and Confirm Password Missmatch.");
        return false;  
        }
    $.ajax({
            url:'<?=$url;?>customerRegister',
            data:
                {
                merchant_keys:'<?=$merchant_keys;?>',
                device_id:'<?=$device_id;?>',
                device_platform:'<?=$device_platform;?>',
                device_uiid:'<?=$device_uiid;?>',
                code_version:'<?=$code_version;?>',
                lang:'<?=$lang;?>',
                search_mode:'<?=$search_mode;?>',
                location_mode:'<?=$location_mode;?>',
                next_step:null,
                first_name:name.value,
                last_name:lname.value,
                email_address:email.value,
                password:password.value,
                contact_phone:phone.value,
                cpassword:password_confirmation.value,
                },
            dataType:'json',
            success:function(result)
                {
                if(result.code == 1)
                    {
                    $('#register_form_msg').show();
                    $('#register_form_msg_error').html('');
                    $('#register_form_msg_error').html(result.msg);
                    document.getElementById("register_form").reset();
                    $('#register_model').hide();
                    }
                else
                    {
                    $('#register_form_msg').show();
                    $('#register_form_msg_error').html('');
                    $('#register_form_msg_error').html(result.msg);
                    return false;
                    }
                }
        });
    }
</script>
